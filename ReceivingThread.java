/**
 * 
 */
package IAG0010JavaClient;

/**
 * @author sergii.ignatov
 *
 */

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.ParseException;

public class ReceivingThread extends Thread {

	public ReceivingThread() {
		this.start();
	}
	
	@Override
	public void run () {
		try {
			Main.socketClient.receiveHello();
			while (true) {
				Main.semToRecvPacket.acquire();
				Main.fileClient.writeInFile(Main.socketClient.receivePacket());
				Main.semToSendReady.release();
			}
		}
		catch (SocketTimeoutException ex) {
			System.out.println("The document has been received");
			Main.windowControl.writeInTextArea("The document has been received");
			System.out.println("Opening the file...");
			Main.windowControl.writeInTextArea("Opening the file...");
			Main.fileClient.launchFile();
		}
		catch (InterruptedException ex) {
			System.out.println("receivingThread was interrupted");
			Logger.getLogger(ReceivingThread.class.getName()).log(Level.SEVERE, null, ex);
			return;
		}
		catch (SocketException ex) {
			if (this.isInterrupted())
				return;
			Logger.getLogger(ReceivingThread.class.getName()).log(Level.SEVERE, null, ex);
			Main.windowControl.informConnectionAbort();
		}
		catch (IOException ex) {
			Logger.getLogger(ReceivingThread.class.getName()).log(Level.SEVERE, null, ex);
			Main.windowControl.informFailedSystem();
		}
		catch (ParseException ex) {
			Logger.getLogger(ReceivingThread.class.getName()).log(Level.SEVERE, null, ex);
			Main.windowControl.informFailedSystem();
		}
		
	}
}
