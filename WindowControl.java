/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package IAG0010JavaClient;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class WindowControl extends JFrame {
	
    private JScrollPane jScrollPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    private JPanel panelConnection = new JPanel();
    private JPanel panelFile = new JPanel();
    private final JTextArea textArea = new JTextArea("Welcome to IAG0010JavaClient\n", 20, 35);
    private final JButton btnConnect = new JButton("Connect");
    private final JButton btnDisconnect = new JButton("Disconnect");
    private final JButton btnOpen = new JButton("Open");
    private final JButton btnClose = new JButton("Close");
    private final JButton btnExit = new JButton("Exit");

    public WindowControl() {
        // Configuration fenêtre
        this.setTitle("IAG0010Client");
        this.setSize(600, 700);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            // windowsClosing when the X button is clicked.
            @Override
            public void windowClosing(WindowEvent e) {
                exitApplication();
            }
        });
        this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        
        btnConnect.setEnabled(false);
        btnDisconnect.setEnabled(false);
        btnClose.setEnabled(false);

        
        //Button Listeners
        //Connect button
        btnConnect.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent arg0) {
        		try{
        			//Establish connection to Server
        			System.out.println("Connect");
        			Main.socketClient = new SocketClient();
        			//after connection - disable Connect button and enable Disconnect
        			btnConnect.setEnabled(false);
        			btnDisconnect.setEnabled(true);
        			//Threads decl
        			Main.receivingThread = new ReceivingThread();
        			Main.sendingThread = new SendingThread();
        			}
        		catch (ConnectException ex) {
        			//Show dialog window if Error occurred
        			JOptionPane.showMessageDialog(WindowControl.this, "Check that the server is listening the network.", "Connection impossible !", JOptionPane.ERROR_MESSAGE);
                    System.out.println("Impossible to connect the client to the server !\nCheck that the server is listening the network.");
        		}
        		catch (UnknownHostException ex) {
        			Logger.getLogger(WindowControl.class.getName()).log(Level.SEVERE, null, ex);
        		}
        		catch (SocketException ex){
        			Logger.getLogger(WindowControl.class.getName()).log(Level.SEVERE, null, ex);
        		}
        		catch (IOException ex) {
        			Logger.getLogger(WindowControl.class.getName()).log(Level.SEVERE, null, ex);
        		}
        	}
        });
        //DisconnectButton
        btnDisconnect.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent mouseEvent) {
        		System.out.println("Disconnect");
        		WindowControl.this.writeInTextArea("Disconnecting");
        		Main.socketClient.close();
        		if (Main.socketClient.isConnected()){
        			WindowControl.this.writeInTextArea("Client is disconnected form the server");
        			btnDisconnect.setEnabled(false);
        			btnConnect.setEnabled(true);
        		}
        	}
        });
        
        //Open Button
        btnOpen.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent mouseEvent) {
        		try {
        			System.out.println("Open");
        			JFileChooser fileChooser = new JFileChooser();
        			fileChooser.setDialogTitle("Choose directory to download file into");
        			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        			fileChooser.showOpenDialog(WindowControl.this);
        			Main.fileClient = new FileClient(fileChooser.getSelectedFile()+File.separator+"IAG0010Received.doc");
        			btnConnect.setEnabled(true);
        			btnOpen.setEnabled(false);
        		}
        		catch (IOException ex){
        			Logger.getLogger(WindowControl.class.getName()).log(Level.SEVERE, null, ex);
        			WindowControl.this.informDirectoryNotExist();
        		}
        		
        	}
        });
        //Close socket client button
        btnClose.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent mouseEvent) {
        		try {
        			Main.fileClient.close();
        			btnClose.setEnabled(false);
        			btnOpen.setEnabled(true);
        		}
        		catch (IOException ex){
        			Logger.getLogger(WindowControl.class.getName()).log(Level.SEVERE, null, ex);
        		}
        	}
        });
        btnExit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                exitApplication();
            }
        });
        
        this.getContentPane().add(jScrollPane);
        textArea.setPreferredSize(new Dimension(200, 350));
        
        jScrollPane.setViewportView(textArea);

        panelConnection.setLayout(new FlowLayout(FlowLayout.CENTER, 50, 5));
        panelConnection.setBorder(BorderFactory.createTitledBorder("Connection"));
        this.getContentPane().add(panelConnection);
        panelConnection.add(btnConnect);
        panelConnection.add(btnDisconnect);

        panelFile.setLayout(new FlowLayout(FlowLayout.CENTER, 50, 5));
        panelFile.setBorder(BorderFactory.createTitledBorder("Data File"));
        this.getContentPane().add(panelFile);
        
        
        panelFile.add(btnOpen);
        
        panelFile.add(btnClose);
        btnExit.setAlignmentX(Component.CENTER_ALIGNMENT);
        btnExit.setHorizontalTextPosition(SwingConstants.CENTER);
        
        getContentPane().add(btnExit);

        this.pack();
        this.setVisible(true);
    }

    // This funciton is called by the ReceivingThread just before completing.
    public void informEndOfDownloading() {
        btnConnect.setEnabled(true);
        btnDisconnect.setEnabled(false);
        btnClose.setEnabled(true);

    }

    // This function permit to write a string in the JTextArea.
    public void writeInTextArea(String addString) {
        System.out.println("String add in the text area.");
        this.textArea.append(addString + "\n");
        this.textArea.setCaretPosition(this.textArea.getText().length());
    }

    private void closeApplication() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        if (Main.socketClient != null) {
            Main.socketClient.close(); // Closing of the socket and interruption of sending and receiving threads.
        }
        if (Main.fileClient != null) {
            try {
                Main.fileClient.close(); // Closing of the file.
            } catch (IOException ex) {
                Logger.getLogger(WindowControl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } // Closing of the file.
        WindowControl.this.dispose();
    }

    // This function is called when the buttonExit is clicked.
    public void exitApplication() {
        WindowControl.this.writeInTextArea("Exit system");
        // Opening of a JOptionPane to confirm Exit.
        int result = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit the application?", "Exit IAG0010Client", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            this.closeApplication();
        }
    }
    
    
    // Exception handle
    
    
    // Called when a IOException for the file is raised.
    public void informDirectoryNotExist(){
        JOptionPane.showMessageDialog(this, "The previous directory doesn't exist.\nYou should select an existing directory.", "Directory doesn't exist !", JOptionPane.ERROR_MESSAGE);
    }
    
    // Called when a SocketException is raised
    public void informConnectionAbort(){
        this.informEndOfDownloading();
        JOptionPane.showMessageDialog(this, "The connection has been interrupted.\nYou should reconnect the Client to the server.", "Connection abort", JOptionPane.ERROR_MESSAGE);
    }
    
    // Called when a IOException for the socket is raised.
    // This Exception shouldn't be raised
    public void informFailedSystem(){
        this.informEndOfDownloading();
        JOptionPane.showMessageDialog(this, "Due to an issue, IAG0010Client restart.", "IAG0010Client Failed", JOptionPane.ERROR_MESSAGE);
    }
}
