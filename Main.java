package IAG0010JavaClient;

import java.util.concurrent.Semaphore;


public class Main {

	public static SocketClient socketClient;
	public static FileClient fileClient;
	public static Semaphore semToSendReady = new Semaphore(0);
	public static Semaphore semToRecvReady = new Semaphore(0);
	public static SendingThread sendingThread;
	public static ReceivingThread receivingThread;
	public static WindowControl windowControl;
	
	
	public static void main (String[] args) throws Exception {
		windowControl = new WindowControl();
	}
	
}
