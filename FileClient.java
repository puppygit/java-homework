/**
 * 
 */
package IAG0010JavaClient;

/**
 * @author sergii.ignatov
 *
 */

import java.awt.Desktop;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class FileClient {

	private File file;
	private BufferedOutputStream out_file;
	
	
	//Create file
	FileClient(String path) throws IOException{
		file = new File(path);
		file.createNewFile();
		System.out.println("File will be saved at: \n"+path);
		Main.windowControl.writeInTextArea("File will be saved at: \n"+path);
		out_file = new BufferedOutputStream(new FileOutputStream(file, true), 2048);
	}
	
	//write buffer contents into file
	public void writeInFile(byte[] buffer){
		try {
			out_file.write(buffer, 4, Convertor.GetIntFromByteArray(buffer, 0));
		}
		catch (IOException ex) {
			Logger.getLogger(FileClient.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	//close file
	public void close() throws IOException{
		out_file.close();
		Main.fileClient = null;
		System.out.println("CLose file");
		Main.windowControl.writeInTextArea("Close file");
	}
	public void launchFile(){
		if (this.file.getPath() == null)
			throw new NullPointerException();
		if (!Desktop.isDesktopSupported())
			return;
		Desktop desktop = Desktop.getDesktop();
		try {
			desktop.open(this.file);
			Main.windowControl.writeInTextArea("Application will automatically launch the file");
			System.out.println("Application will automatically launch the file");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
