/**
 * 
 */
package IAG0010JavaClient;

/**
 * @author sergii.ignatov
 *
 */

import java.io.*;
import java.net.*;
import java.text.ParseException;
import java.util.logging.*;


public class SocketClient {

	private String address = "localhost";
	private int port = 1234;
	private Socket socket;
	private BufferedOutputStream out_socket;
	private BufferedInputStream in_socket;
	
	SocketClient() throws ConnectException, UnknownHostException, SocketException, IOException {
		
		socket = new Socket(address, port);
		out_socket = new BufferedOutputStream(socket.getOutputStream(), 44);
		in_socket = new BufferedInputStream(socket.getInputStream(), 2048);
		socket.setSoTimeout(6000);
		System.out.println("The download will start now.\nThe downloaded file will be launched automatically");
		Main.appWindow.writeInTextArea("The download will start now.\nThe downloaded file will be launched automatically");
		
	}
	
	public void close() {
		
		try {
			if(Main.recvThread.isAlive()){
				Main.recvThread.interrupt();
			}
			if(Main.sendThread.isAlive()){
				Main.sendThread.interrupt();
			}
			socket.close();
		}
		catch (IOException ex) {
			Logger.getLogger(SocketClient.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public boolean isConnected() {
		
		return socket.isConnected();
	}
	
	private void send(String sendText, int lenText) throws IOException {
		
		byte[] buffer = new byte[lenText+4];
		Convertor.PutIntIntoByteArray(buffer, 0, lenText);
		Convertor.PutStringIntoByteArray(buffer, 4, sendText);
		out_socket.write(buffer, 0, lenText+4);
		System.out.println("Message sent: "+sendText);
	}
	
	public void sendHello() throws IOException{
		
		send("Hello IAG0010Server", 40);
	}
	
	public void sendReady() throws IOException{
		
		send("Ready", 40);
	}
	
	public void recvHello() throws IOException, ParseException {
		
		String recvText;
		int lenText;
		int lenMessage = 44;
		byte[] buffer = new byte[lenMessage];
		in_socket.read(buffer, 0, lenMessage);
		lenText = Convertor.GetIntFromByteArray(buffer, 0);
		recvText = Convertor.GetStringFromByteArray(buffer, 4, lenMessage);
		System.out.println("Text length: "+lenText+"\nMessage received: "+recvText);
		
	}
	
	public byte[] recvPacket() throws ParseException, IOException {
		int lenText;
		int lenMessage = 2048;
		byte[] buffer = new byte[lenMessage];
		in_socket.read(buffer, 0, lenMessage);
		lenText = Convertor.GetIntFromByteArray(buffer, 0);
		System.out.println(lenText+" bytes received");
		Main.appWindow.writeInTextArea(lenText+" bytes received");
		
		return buffer;
	}
}
