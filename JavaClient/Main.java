/**
 * 
 */
package IAG0010JavaClient;

/**
 * @author sergii.ignatov
 *
 */

import java.util.concurrent.Semaphore;

public class Main {
	
	public static SocketClient sockClient;
	public static FileClient fileClient;
	public static SendingThread sendThread;
	public static ReceivingThread recvThread;
	public static AppWindow appWindow;
	public static Semaphore semToSendReady = new Semaphore(0);
	public static Semaphore semToReceivePacket = new Semaphore(0);
	
	public static void main (String[] args) throws Exception {
		appWindow = new AppWindow();
	}

}
