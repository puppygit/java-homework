/**
 * 
 */
package IAG0010JavaClient;

/**
 * @author sergii.ignatov
 *
 */
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReceivingThread extends Thread{
	public ReceivingThread(){
        this.start();
    }
    
    @Override
    public void run(){
        try {
            Main.sockClient.recvHello();
            while(true){
                Main.semToReceivePacket.acquire();
                Main.fileClient.writeInFile(Main.sockClient.recvPacket());
                Main.semToSendReady.release();
            }
        } catch (SocketTimeoutException ex) {
            System.out.println("All the document received.");
            Main.appWindow.writeInTextArea("All the document received.");
            System.out.println("Opening of the received file in a default application.");
            Main.appWindow.writeInTextArea("Opening of the received file in a default application.");
            Main.fileClient.launchFileWithDefaultApplication();
        } catch (InterruptedException ex) {
            System.out.println("The receivingThread has been interrupted");
            Logger.getLogger(ReceivingThread.class.getName()).log(Level.SEVERE, null, ex);
            return;
        } catch (ParseException ex) {
            Logger.getLogger(ReceivingThread.class.getName()).log(Level.SEVERE, null, ex);
            Main.appWindow.informFailedSystem();
        } catch (SocketException ex) {
            if(this.isInterrupted())
                return;
            Logger.getLogger(ReceivingThread.class.getName()).log(Level.SEVERE, null, ex);
            Main.appWindow.informConnectionAbort();
        } catch (IOException ex) {
            Logger.getLogger(ReceivingThread.class.getName()).log(Level.SEVERE, null, ex);
            Main.appWindow.informFailedSystem();
        }
        if(Main.sockClient != null)
            Main.sockClient.close();
        Main.appWindow.informEndOfDownloading();
    }
}
