/**
 * 
 */
package IAG0010JavaClient;

/**
 * @author sergii.ignatov
 *
 */

import java.io.IOException;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SendingThread extends Thread{
	
	public SendingThread(){
        
		this.start();
    }
    
    @Override
     public synchronized void run() {
        try {
            Main.sockClient.sendHello();
            Main.semToReceivePacket.release();
            while(true){
                Main.semToSendReady.acquire();
                Main.sockClient.sendReady();
                Main.semToReceivePacket.release();
            }
        } catch (InterruptedException ex) {
            System.out.println("The sendingThread has been interrupted");
            return;
        } catch (SocketException ex) {
            if(this.isInterrupted())
                return;
            Logger.getLogger(SendingThread.class.getName()).log(Level.SEVERE, null, ex);
            Main.appWindow.informConnectionAbort();
        } catch (IOException ex) {
            Logger.getLogger(SendingThread.class.getName()).log(Level.SEVERE, null, ex);
            Main.appWindow.informFailedSystem();
        }
        if(Main.sockClient != null)
            Main.sockClient.close();
    }    
}
