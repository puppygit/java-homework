package IAG0010JavaClient;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SocketClient {
	
	//Attributes
	private String address = "localhost";
	private int port = 1234;
	private Socket socket;
	private BufferedOutputStream out_socket;
	private BufferedInputStream in_socket;
	
	//Initialize
	SocketClient() throws ConnectException, UnknownHostException, SocketException, IOException{
		
		socket = new Socket(address, port);
		out_socket = new BufferedOutputStream(socket.getOutputStream(), 44);
		in_socket = new BufferedInputStream(socket.getInputStream(), 2048);
		socket.setSoTimeout(6000);
		System.out.println("Download will start now.\nThe downloaded file will be launched automatically");
		Main.windowControl.writeInTextArea("Download will start now.\nThe downloaded file will be launched automatically");
		
	}

	// Close Socket
	public void close(){
		try {
			if (Main.receivingThread.isAlive())
				Main.receivingThread.interrupt();
			if (Main.sendingThread.isAlive())
				Main.sendingThread.interrupt();
			socket.close();
		}
		catch (IOException ex) {
			Logger.getLogger(SocketClient.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public boolean isConnected (){
		return socket.isConnected();
	}
	
	//Send message
	private void send(String sendText, int lenText) throws IOException{
		//set buffer size
		byte[] buffer = new byte[lenText+4];
		Convertor.PutIntIntoByteArray(buffer,0 , lenText);
		Convertor.PutStringIntoByteArray(buffer, 4, sendText);
		out_socket.write(buffer, 0, lenText+4);
		System.out.println("Message sent: "+sendText);
	}
	
	//send Hello to Server
	public void sendHello () throws IOException{
		send("Hello IAG0010Server", 40);
	}
	
	
	//send Ready message
	public void sendReady() throws IOException{
		send("Ready", 40);
	}
	
	//Receive Hello message from Server
	public void receiveHello() throws ParseException, IOException{
		String recvText;
		int lenText;
		int lenMessage = 44;
		byte[] buffer = new byte[lenMessage];
		in_socket.read(buffer, 0, lenMessage);
		lenText = Convertor.GetIntFromByteArray(buffer, 0);
		recvText = Convertor.GetStringFromByteArray(buffer, 4, lenMessage);
		System.out.println("text length: "+lenText+"\nMessage received: "+recvText);
	}
	
	public byte[] receivePacket() throws ParseException, IOException{
		int lenText;
		int lenMessage = 44;
		byte[] buffer = new byte[lenMessage];
		in_socket.read(buffer, 0, lenMessage);
		lenText = Convertor.GetIntFromByteArray(buffer, 0);
		System.out.println(lenText+" bytes received");
		Main.windowControl.writeInTextArea(lenText+" bytes received");
		return buffer;
	}
}
